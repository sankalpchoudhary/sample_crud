var express = require('express');
const heartbeat = require('../model/heartbeat');
var router = express.Router();

const ApplicationCtrl = require('../controller/ApplicationController')

/***********************************
Ping API's 
*/

router.get('/ping', ApplicationCtrl.ping);




module.exports = router;