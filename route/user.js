var express = require('express');
var router = express.Router();

const UserCtrl = require('../controller/UserController')

/***********************************
User API's 
Add User
List User
Get User
*/

router.post('/add_user', UserCtrl.create_user);
router.post('/get_user', UserCtrl.get_user);
router.post('/user_list', UserCtrl.list_user);

module.exports = router;