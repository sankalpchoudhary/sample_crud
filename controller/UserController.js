const { Validator } = require('node-input-validator');

const UserService = require('../services/UserService')
const { response, hash, checkPassword } = require('../Helper/Service');
const { SUCCESS, ERROR } = require('../localization/Messges/MessageList');

const winston = require('winston');
const { lookup } = require('geoip-lite');

const logger = winston.createLogger({
    level: 'error',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
        new winston.transports.File({ filename: 'log/error/user/user_error.log', level: 'error' }),
    ],
});

logger.add(new winston.transports.Console({
    format: winston.format.simple(),
}));

module.exports.create_user = async (req, res, next) => {
    try {
        const v = new Validator(req.body, {
            email: 'required|email',
            password: 'required'
        });

        const matched = await v.check();
        if (!matched) {
            return res
                .status(422)
                .json({
                    success: true,
                    data: v.errors,
                    message: "Please Enter Valid Email/Password",
                });
        }

        const UserFindData = {
            email: req.body.email
        }

        const UserCheckResponse = await UserService.getOne(UserFindData)

        if (UserCheckResponse) {
            return res.status(404).json(response(ERROR.USER_ALREADY_REGISTRED, [], false))
        }

        const UserAddData = {
            email: req.body.email,
            password: hash(req.body.password)
        }

        await UserService.add(UserAddData)
        return res.status(200).json(response(SUCCESS.USER_CREATED_SUCESS_MESSAGE))
    } catch (error) {
        logger.log(
            {
                level: "error",
                message: "Error in creating user",
                error,
                error_message: error.message,
                error_stack: error.stack
            }
        )
        return res.status(404).json(response(ERROR.DEFAULT_ERROR_MESSAGE, [], false))
    }
}

module.exports.get_user = async (req, res, next) => {
    try {
        const v = new Validator(req.body, {
            email: 'required|email',
            password: 'required'
        });

        const matched = await v.check();
        if (!matched) {
            return res
                .status(422)
                .json({
                    success: true,
                    data: v.errors,
                    message: "Please Enter Valid Email/Password",
                });
        }

        const UserGetData = {
            email: req.body.email,
        }

        const UserResponse = await UserService.getOne(UserGetData)

        if (!(UserResponse)) {
            return res.status(404).json(response(ERROR.WRONG_EMAIL_PASSWORD, [], false))
        }

        const VerifyPassword = checkPassword(req.body.password, UserResponse.password)
        if (VerifyPassword) {
            const UserFrontResponse = {
                email: UserResponse.email,
                createdAt: UserResponse.createdAt,
            }
            return res.status(200).json(response('Data retrieved successfully', UserFrontResponse))
        } else {
            return res.status(404).json(response(ERROR.WRONG_EMAIL_PASSWORD, [], false))
        }
    } catch (error) {
        logger.log(
            {
                level: "error",
                message: "Error in getting user",
                error,
                error_message: error.message,
                error_stack: error.stack
            }
        )
        return res.status(404).json(response(ERROR.DEFAULT_ERROR_MESSAGE, [], false))
    }
}

module.exports.list_user = async (req, res, next) => {
    try {
        // Set Paginationg Params
        req.body.limit = req.body.limit == undefined ? 10 : req.body.limit;
        req.body.page = (req.body.page == undefined || req.body.page == "") ? 1 : req.body.page;
        req.body.search = req.body.search == undefined ? "" : req.body.search;
        req.body.sortDirection =
            (req.body.sortDirection == undefined || req.body.sortDirection == "")
                ? -1
                : req.body.sortDirection;
        req.body.sortyBy =
            (req.body.sortyBy == undefined || req.body.sortyBy == "")
                ? "createdAt"
                : req.body.sortyBy;

        const ListParams = {
            limit: req.body.limit,
            page: req.body.page,
            search: req.body.search,
            sortDirection: req.body.sortDirection,
            sortyBy: req.body.sortyBy
        }

        // Pagination Response
        const UserListResponse = await UserService.list(ListParams)

        return res.status(200).json(response(SUCCESS.DEFAULT_SUCCESS_MESSAGE, UserListResponse))
    } catch (error) {
        logger.log(
            {
                level: "error",
                message: "Error in listing user",
                error,
                error_message: error.message,
                error_stack: error.stack
            }
        )
        return res.status(404).json(response(ERROR.DEFAULT_ERROR_MESSAGE, [], false))
    }
}