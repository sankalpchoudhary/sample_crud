const heartbeat = require('../model/heartbeat');

var os = require('os');

const winston = require('winston');
const { lookup } = require('geoip-lite');
const { response } = require('../Helper/Service');
const { SUCCESS, ERROR } = require('../localization/Messges/MessageList');

const logger = winston.createLogger({
    level: 'error',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
        new winston.transports.File({ filename: 'log/error/application/ping_error.log', level: 'error' }),
    ],
});

module.exports.ping = async (req, res, next) => {
    try {
        const DBResponse = await heartbeat.findOne({}, {
            _id: 0,
            is_OK: 1
        })

        const Totalmemory = os.totalmem() / 1048576
        const FreeMem = os.freemem() / 1048576

        // Check How much memory free in Percentage If less then 10 % the show system critical else show system normal
        const memoryUsage = (FreeMem / Totalmemory) * 100

        const SystemMemopryStatus =  (memoryUsage < 10) ? "Critical" : "Normal"
        
        const healthcheck = {
            uptime: process.uptime(),
            message: 'OK',
            db_status: {
                db_status: "UP",
                db_response: DBResponse
            },
            status:SystemMemopryStatus,
            timestamp: Date.now()
        };
        return res.status(200).json(response(SUCCESS.DEFAULT_SUCCESS_MESSAGE, healthcheck))
    } catch (error) {
        logger.log(
            {
                level: "error",
                message: "Error in getting user",
                error,
                error_message: error.message,
                error_stack: error.stack
            }
        )
        return res.status(503).json(response(ERROR.DEFAULT_ERROR_MESSAGE, [], false))
    }
}