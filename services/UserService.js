const User = require("../model/User");
const mongoose = require('mongoose');

module.exports = class UserService {
    static async add(data) {
        try {
            const user_create = await User.create(data)
            return user_create
        } catch (error) {
            throw new Error(error)
        }
    }

    static async getOne(data) {
        try {
            const value = await User.findOne(data)
            return value
        } catch (error) {
            throw new Error(error)
        }
    }

    static async list(Params) {
        try {
            console.log("Params", Params);
            const options = {
                page: Params.page || 0,
                limit: Params.limit || 2,
            };

            let regex = new RegExp(Params.search, "i");

            var aggregation_pipeline = [
                {
                    $match: {
                        $and: [
                            {
                                $or: [
                                    { email: regex },
                                ],
                            },
                        ],
                    },
                },
                {
                    $project: {
                        _id: 1,
                        email: 1,
                        createdAt: 1,
                        updatedAt: 1,
                    },
                },
                {
                    $sort: {
                        [Params.sortyBy]: Params.sortDirection,
                    },
                }
            ]
            var aggregate = User.aggregate(aggregation_pipeline);
            var result = await User.aggregatePaginate(aggregate, options);
            return result;
        } catch (error) {
            return error;
        }
    }
}