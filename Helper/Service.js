const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = class Service {
    static token_generator(token_data) {
        const token = jwt.sign(
            token_data,
            process.env.SECRET,
            // Set Token Expire Time
            // {
            //     expiresIn: parseInt(process.env.EXPIRE_TOKEN)
            // }
        );
        return token
    }

    static response(message, data = [], status = true) {
        return {
            success: status,
            data: data,
            message: message,
        }
    }

    static hash(data) {
        const salt = bcrypt.genSaltSync(saltRounds)
        return bcrypt.hashSync(data, salt)
    }

    static checkPassword(password, hash) {
        // console.log(password, hash);
        return bcrypt.compareSync(password, hash)
    }
}