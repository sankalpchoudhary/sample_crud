const heartbeat = require("../model/heartbeat");
const mongoose = require('mongoose');
require('dotenv').config();
const Schema = mongoose.Schema;

mongoose.connect(process.env.MONGO_DB_CONNECT, {
    useNewUrlParser: true // Boilerplate for Mongoose 5.x
});

const data = {
    is_OK: 1,
}

async function adminseeder(info) {
    try {
        await heartbeat.deleteMany()
        console.log("Delete old heartbeat record");
        await heartbeat.create(info)
        console.log("Create heartbeat through seeder file");
        process.exit()
    } catch (error) {
        console.log("Error in Creating heartbeat", error);
        process.exit()
    }
}


adminseeder(data)