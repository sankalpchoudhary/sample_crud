var express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();
const cors = require('cors')
const winston = require('winston');
const { lookup } = require('geoip-lite');

// Application Routes
const UserRoute = require('./route/user')
const ApplicationRoute = require('./route/application')

const APIVERSION = process.env.APIVERSION ? process.env.APIVERSION : "/api/v1"
const PORT = process.env.PORT ? process.env.PORT : 5000

var app = express();

app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const logger404 = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'app-service' },
    transports: [
        new winston.transports.File({ filename: 'log/error/404Error.log', level: 'error' }),
    ],
});

logger404.add(new winston.transports.Console({
    format: winston.format.simple(),
}));

// Demonstrate the readyState and on event emitters
console.log(mongoose.connection.readyState); //logs 0
mongoose.connection.on('connecting', () => {
    console.log('connecting')
    console.log(mongoose.connection.readyState); //logs 2
});
mongoose.connection.on('connected', () => {
    console.log('connected');
    console.log(mongoose.connection.readyState); //logs 1
});
mongoose.connection.on('disconnecting', () => {
    console.log('disconnecting');
    console.log(mongoose.connection.readyState); // logs 3
});
mongoose.connection.on('disconnected', () => {
    console.log('disconnected');
    console.log(mongoose.connection.readyState); //logs 0
});
// Connect to a MongoDB server running on 'localhost:27017' and use the
// 'test' database.
mongoose.connect(process.env.MONGO_DB_CONNECT, {
    useNewUrlParser: true // Boilerplate for Mongoose 5.x
});

app.use(`${APIVERSION}/application`, ApplicationRoute);
app.use(`${APIVERSION}/user`, UserRoute);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    const ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress;

    logger404.log({
        level: 'error',
        message: 'User does not found App Route',
        data: {
            userIP: ip,
            GeoIPInfo: lookup(ip)
        }
    });

    res.status(404).json({ success: false, data: [], message: "App does not have this route" })
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    console.log(err);
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

app.listen(PORT, (err) => {
    console.log(
        `Listining on PORT ${process.env.PORT}`);
})