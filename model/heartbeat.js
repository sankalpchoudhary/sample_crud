const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const heartbeatSchema = Schema({
    is_OK: {
        type: Boolean,
        default: 1,
    },
}, {
    timestamps: true
});

module.exports = heartbeat = mongoose.model("heartbeat", heartbeatSchema);
