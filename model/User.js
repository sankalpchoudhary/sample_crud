const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const userSchema = Schema({
    email: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    is_active: {
        type: Boolean,
        default: 1,
    },
    is_deleted: {
        type: Boolean,
        default: 0,
    },
}, { timestamps: true });



userSchema.plugin(aggregatePaginate);
module.exports = user = mongoose.model("user", userSchema);
