// Connect to a MongoDB server
mongoose.connect(process.env.MONGO_DB_CONNECT, {
    useNewUrlParser: true
});

// Demonstrate the readyState and on event emitters
console.log(mongoose.connection.readyState); //logs 0
mongoose.connection.on('connecting', () => {
    console.log('connecting')
    console.log(mongoose.connection.readyState); //logs 2
});
mongoose.connection.on('connected', () => {
    console.log('connected');
    console.log(mongoose.connection.readyState); //logs 1
});
mongoose.connection.on('disconnecting', () => {
    console.log('disconnecting');
    console.log(mongoose.connection.readyState); // logs 3
});
mongoose.connection.on('disconnected', () => {
    console.log('disconnected');
    console.log(mongoose.connection.readyState); //logs 0
});
