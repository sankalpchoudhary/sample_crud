## Sample Crud Operation Project

How to setup Project

> Run This command to create heartbeat Collection in Databasw

```
npm run init
```

> Here are Basic .env File for Project

```
MONGO_DB_CONNECT = "mongodb://127.0.0.1:27017/crud"
APIVERSION = "/api/v1"
PORT = 5000
```

> Last you can run project by

```
npm run start
```


Click on link to add postman collection

> [Postman Link](https://www.getpostman.com/collections/8df32e55f32740d0cfce)

Hosted On Heroku

```
BASE_URL = https://samplecruddemo.herokuapp.com/api/v1
BASE_URL = https://127.0.0.1:5000/api/v1
```

> API END POINTS

1. Create User (POST) => {{BASE_URL}}/user/add_user

2. Get User (POST) => {{BASE_URL}}/user/get_user

3. List User (POST) => {{BASE_URL}}/user/user_list

4. Ping Server (GET) => {{BASE_URL}}/application/ping


> Try Ping API Directly On Browser
```
https://samplecruddemo.herokuapp.com/api/v1/application/ping
```